/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buscaminas.control;

import buscaminas.model.Tauler;
import buscaminas.vista.Principal;
import buscaminas.vista.Principal.Boto;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.ImageIcon;
import javax.swing.SwingUtilities;

/**
 *
 * @author alumneDAM
 */

public class Joc implements ActionListener, MouseListener {

	private Principal principal;
	private Tauler tauler;
	int cont = 0;

	public Joc() {
		tauler = new Tauler(Principal.FILES, Principal.COLUMNES, Principal.BOMBES);
		this.principal = new Principal(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == principal.itemPrincipiant) {
			principal.panellGraella.removeAll();
			principal.FILES = 10;
			principal.COLUMNES = 10;
			principal.BOMBES = 20;
			principal.configGraella();
			principal.omplirGraella();
			tauler = new Tauler(Principal.FILES, Principal.COLUMNES, Principal.BOMBES);
			principal.panellGraella.updateUI();

		} else if (e.getSource() == principal.itemIntermedi) {
			principal.panellGraella.removeAll();
			principal.FILES = 15;
			principal.COLUMNES = 15;
			principal.BOMBES = 45;
			principal.configGraella();
			principal.omplirGraella();
			tauler = new Tauler(Principal.FILES, Principal.COLUMNES, Principal.BOMBES);
			principal.panellGraella.updateUI();
		} else if (e.getSource() == principal.itemExpert) {
			principal.panellGraella.removeAll();
			principal.FILES = 20;
			principal.COLUMNES = 20;
			principal.BOMBES = 80;
			principal.configGraella();
			principal.omplirGraella();
			tauler = new Tauler(Principal.FILES, Principal.COLUMNES, Principal.BOMBES);
			principal.panellGraella.updateUI();
		} else if (e.getSource() == principal.itemPersonalitzat) {
			principal.configPersonalitzat();
		} else if (e.getSource() == principal.botoAcceptar) {
			principal.panellGraella.removeAll();
			Principal.FILES = Principal.getFilaSpinner();
			Principal.COLUMNES = Principal.getColumnaSpinner();
			Principal.BOMBES = Principal.getBombaSpinner();
			principal.configGraella();
			principal.omplirGraella();
			tauler = new Tauler(Principal.FILES, Principal.COLUMNES, Principal.BOMBES);
			principal.panellGraella.updateUI();
			principal.personalitzat.dispose();
		} else if (e.getSource() == principal.botoCancel) {
			principal.personalitzat.dispose();
		} else if (e.getSource() == principal.botoReiniciar) {
			reiniciar();
		} else if (e.getSource() == principal.itemSortir) {
			System.exit(0);
		}
	}

	public void reiniciar() {
		principal.panellBotons.removeAll();
		principal.configPanellBotons();
		principal.panellGraella.removeAll();
		principal.configGraella();
		principal.omplirGraella();
		principal.panellGraella.updateUI();
		principal.panellBotons.updateUI();
		tauler = new Tauler(Principal.FILES, Principal.COLUMNES, Principal.BOMBES);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		Boto boto = (Boto) e.getSource();
		int fila = boto.getFila();
		int columna = boto.getColumna();
		ImageIcon imagen = new ImageIcon("img/0.png");
		ImageIcon imagenE = new ImageIcon("img/1.png");
		imagenE = new ImageIcon(imagenE.getImage().getScaledInstance(40, 40, Image.SCALE_AREA_AVERAGING));
		imagen = new ImageIcon(imagen.getImage().getScaledInstance(40, 40, Image.SCALE_AREA_AVERAGING));
		if (SwingUtilities.isLeftMouseButton(e)) {
			if (tauler.hihaBomba(fila, columna)) {
				tauler.destaparCasella(fila, columna);
				boto.setIcon(imagen);
				for (int i = 0; i < principal.FILES; i++) {
					for (int j = 0; j < principal.COLUMNES; j++) {
						tauler.destaparCasella(i, j);
						if (tauler.hihaBomba(i, j)) {
							imagen = new ImageIcon(
									imagen.getImage().getScaledInstance(40, 40, Image.SCALE_AREA_AVERAGING));
							principal.botons[i][j].setIcon(imagen);
						} else {
							principal.botons[i][j].setText(String.valueOf(tauler.contaBombes(i, j)));
						}
					}
				}
				principal.mostrarMissatge("HAS PERDIDO");
				reiniciar();
			} else {
				tauler.destaparCasella(fila, columna);
				// ImageIcon imagen = new ImageIcon("img/1.png");
				// imagen = new ImageIcon(imagen.getImage().getScaledInstance(40, 40,
				// Image.SCALE_AREA_AVERAGING));
				// boto.setIcon(imagen);

				boto.setText(String.valueOf(tauler.contaBombes(fila, columna)));
				if (tauler.contaBombes(fila, columna) == 0) {
					for (int i = 0; i < principal.FILES; i++) {
						for (int j = 0; j < principal.COLUMNES; j++) {
							if (tauler.clicat(i, j)) {
								principal.botons[i][j].setText(String.valueOf(tauler.contaBombes(i, j)));
							}
						}
					}
				}
			}
			cont = 0;
			for (int i = 0; i < principal.FILES; i++) {
				for (int j = 0; j < principal.COLUMNES; j++) {
					if (tauler.clicat(i, j)) {
						cont++;
					}
				}
			}

			if ((Principal.FILES * Principal.COLUMNES - Principal.BOMBES) - cont != 0) {
				principal.botoCasellesDestapar
						.setText("" + (((Principal.FILES * Principal.COLUMNES) - Principal.BOMBES) - cont));
			} else {
				principal.mostrarMissatge("HAS GANADO EN HORABUENA");
				reiniciar();
			}
		} else if (SwingUtilities.isRightMouseButton(e) && (boto.getIcon() != new ImageIcon(
				imagenE.getImage().getScaledInstance(40, 40, Image.SCALE_AREA_AVERAGING)))) {
			boto.setIcon(imagenE);
		}

	}

	@Override
	public void mouseEntered(MouseEvent e) {

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

}
