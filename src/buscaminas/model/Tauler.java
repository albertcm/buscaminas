/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buscaminas.model;

import java.util.Random;

/**
 *
 * @author alumneDAM
 */
public class Tauler {
	private Casella[][] caselles;
	private final int FILES, COLUMNES;
	private int bombes;

	public Tauler(int FILES, int COLUMNES, int bombes) {
		this.FILES = FILES;
		this.COLUMNES = COLUMNES;
		this.bombes = bombes;
		caselles = new Casella[FILES][COLUMNES];
		crearCasellas();
		posarBombes();
	}

	public void crearCasellas() {
		for (int i = 0; i < FILES; i++) {
			for (int j = 0; j < COLUMNES; j++) {
				caselles[i][j] = new Casella(false, false, 0);
			}
		}
	}

	public void posarBombes() {

		Random rnd = new Random();
		int cont = 0;
		while (cont < bombes) {
			int posC = rnd.nextInt(COLUMNES);
			int posF = rnd.nextInt(FILES);
			if (!caselles[posF][posC].isEsBomba()) {
				caselles[posF][posC].setEsBomba(true);
				cont++;
			}

		}

		for (int i = 0; i < FILES; i++) {
			for (int j = 0; j < COLUMNES; j++) {
				contaBombes(i, j);
			}
		}
		System.out.println("-------------num bombas-------------");
		for (int i = 0; i < FILES; i++) {
			for (int j = 0; j < COLUMNES; j++) {
				System.out.print(caselles[i][j].getNumBombes() + " ");
			}
			System.out.println();
		}
		System.out.println("-------------hay bombas-------------");
		for (int i = 0; i < FILES; i++) {
			for (int j = 0; j < COLUMNES; j++) {
				System.out.print(caselles[i][j].isEsBomba() + " ");
			}
			System.out.println();
		}
	}

	public int contaBombes(int fila, int col) {
		int numBombas = 0;
		for (int i = fila - 1; i <= fila + 1; i++) {
			for (int j = col - 1; j <= col + 1; j++) {
				if ((i >= 0 && i < FILES) && j >= 0 && j < COLUMNES) {
					if (caselles[i][j].isEsBomba()) {
						numBombas++;
					}
				}
			}

		}

		if (caselles[fila][col].isEsBomba()) {
			numBombas--;
		}
		caselles[fila][col].setNumBombes(numBombas);
		return numBombas;
	}

	public boolean destaparCasella(int fila, int col) {

		if (caselles[fila][col].getNumBombes() > 0 && !caselles[fila][col].isEstat()
				&& !caselles[fila][col].isEsBomba()) {
			caselles[fila][col].setEstat(true);
		} else if (caselles[fila][col].getNumBombes() == 0 && !caselles[fila][col].isEstat()
				&& !caselles[fila][col].isEsBomba()) {
			caselles[fila][col].setEstat(true);
			
			for (int i = fila - 1; i <= fila + 1; i++) {
				for (int j = col - 1; j <= col + 1; j++) {
					if ((i >= 0 && i < FILES) && j >= 0 && j < COLUMNES) {
						destaparCasella(i, j);
					}
				}
			}
		}
		System.out.println("------------estado de casillas--------------");
		for (int i = 0; i < FILES; i++) {
			for (int j = 0; j < COLUMNES; j++) {
				System.out.print(caselles[i][j].isEstat() + " ");
			}
			System.out.println();
		}
		return caselles[fila][col].isEstat();
	}

	public boolean hihaBomba(int fila, int col) {
		return caselles[fila][col].isEsBomba();
	}
	public boolean clicat(int fila, int col) {
		return caselles[fila][col].isEstat();
	}
}
