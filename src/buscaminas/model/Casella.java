/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buscaminas.model;

/**
 * @author alumneDAM
 */
public class Casella {
	private boolean esBomba;
	private boolean estat;
	private int numBombes;
	public Casella(boolean esBomba, boolean estat, int numBombes) {
		super();
		this.esBomba = esBomba;
		this.estat = estat;
		this.numBombes = numBombes;
	}
	public boolean isEsBomba() {
		return esBomba;
	}
	public boolean isEstat() {
		return estat;
	}
	public int getNumBombes() {
		return numBombes;
	}
	public void setEsBomba(boolean esBomba) {
		this.esBomba = esBomba;
	}
	public void setEstat(boolean estat) {
		this.estat = estat;
	}
	
	public void setNumBombes(int numBombes) {
		this.numBombes = numBombes;
	}
	@Override
	public String toString() {
		return "Casella [esBomba=" + esBomba + ", estat=" + estat + ", numBombes=" + numBombes + "]";
	}
	
}
