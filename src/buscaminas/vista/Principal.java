package buscaminas.vista;

import buscaminas.control.Joc;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

/**
 *
 * @author alumneDAM
 */
public class Principal extends JFrame {

	private Joc joc;

	private static int currentValue = 10;

	private static int minValue = 10;

	private static int maxValue = 40;

	private static int steps = 1;
	public static int FILES = 10, COLUMNES = 10, BOMBES = 20;
	static SpinnerNumberModel spinnerFilesModel = new SpinnerNumberModel(currentValue, minValue, maxValue, steps);
	public static JSpinner spinnerFiles = new JSpinner(spinnerFilesModel);
	public static SpinnerNumberModel spinnerColumnesModel = new SpinnerNumberModel(currentValue, minValue, maxValue, steps);
	static JSpinner spinnerColumnes = new JSpinner(spinnerColumnesModel);
	public static SpinnerNumberModel spinnerBombesModel = new SpinnerNumberModel(currentValue, minValue, maxValue, steps);
	public static JSpinner spinnerBombes = new JSpinner(spinnerBombesModel);
	public static JFrame personalitzat;
	private JLabel labelFiles = new JLabel("Files: ");
	private JLabel labelColumnes = new JLabel("Columnes: ");
	private JLabel labelBombes = new JLabel("Bombes: ");
	private JLabel labelTitolPersonalitzat = new JLabel("CONFIGURACIÓ PERSONALITZADA");
	public JButton botoCasellesDestapar;
	public JButton botoReiniciar;
	public static JSpinner.DefaultEditor spinnerEditor, spinnerEditor2, spinnerEditor3;
	public JButton botoAcceptar = new JButton("Acceptar"), botoCancel = new JButton("Cancelar");
	public JPanel panellBotons = new JPanel();
	public JPanel panellPersonalitzatBtns = new JPanel();
	private JPanel panellPersonalitzat = new JPanel();
	public JPanel panellGraella = new JPanel();
	private JMenuBar barraMenu;
	private JMenu menuDif, menuAjuda;
	public JMenuItem itemPrincipiant, itemIntermedi, itemExpert, itemPersonalitzat, itemSortir;
	private FlowLayout flowLayout = new FlowLayout();

	public Principal(Joc joc) {
		this.joc = joc;
		configuracio();
		configMenuBar();
		configPanellBotons();
		configGraella();
		omplirGraella();
		this.pack();
		this.setVisible(true);
	}

	private void configuracio() {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setPreferredSize(new Dimension(700, 600));
		this.setLocationRelativeTo(null);
		this.setLocation(90, 90);
		this.setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
		this.setTitle("BUSCAMINES");
	}

	private void configMenuBar() {
		barraMenu = new JMenuBar();
		menuDif = new JMenu("Dificultat");
		menuAjuda = new JMenu("Ajuda");

		itemPrincipiant = new JMenuItem("Principiant");
		itemPrincipiant.setMnemonic('P');

		itemIntermedi = new JMenuItem("Intermedi");
		itemIntermedi.setMnemonic('I');

		itemExpert = new JMenuItem("Expert");
		itemExpert.setMnemonic('E');

		itemPersonalitzat = new JMenuItem("Personalitzat");
		itemPersonalitzat.setMnemonic('X');

		itemSortir = new JMenuItem("Sortir");
		itemSortir.setMnemonic('S');

		menuDif.add(itemPrincipiant);
		menuDif.add(itemIntermedi);
		menuDif.add(itemExpert);
		menuDif.addSeparator();
		menuDif.add(itemPersonalitzat);
		menuDif.addSeparator();
		menuDif.add(itemSortir);

		barraMenu.add(menuDif);

		this.setJMenuBar(barraMenu);
		itemPrincipiant.addActionListener(joc);
		itemIntermedi.addActionListener(joc);
		itemExpert.addActionListener(joc);
		itemPersonalitzat.addActionListener(joc);
		itemSortir.addActionListener(joc);

	}

	public void configPanellBotons() {
		botoCasellesDestapar = new JButton("X");
		botoReiniciar = new JButton("Reiniciar");
		panellBotons.setBackground(Color.red);
		panellBotons.setLayout(flowLayout);
		flowLayout.setHgap(60);
		flowLayout.setVgap(30);
		panellBotons.add(botoCasellesDestapar);
		panellBotons.add(botoReiniciar);
		panellBotons.setMaximumSize(new Dimension(1600, 300));
		getContentPane().add(panellBotons);
		botoReiniciar.addActionListener(joc);
		
		
	}
	
	public static JComponent editorFiles, editorColumnes, editorBombes;
	public void configPersonalitzat() {
		
		// Configuracio finestra
		personalitzat = new JFrame();
		personalitzat.setDefaultCloseOperation(1);
		personalitzat.setPreferredSize(new Dimension(400, 300));
		personalitzat.setLocationRelativeTo(null);
		personalitzat.setLocation(90, 90);
		personalitzat.setTitle("PERSONALITZAT");
		personalitzat.setResizable(false);
		panellPersonalitzat.setLayout(new GridLayout(4,1));
		// Alinear text dels spinners.
		editorFiles = spinnerFiles.getEditor();
		editorColumnes = spinnerColumnes.getEditor();
		editorBombes = spinnerBombes.getEditor();
		if (editorFiles instanceof JSpinner.DefaultEditor) {
			spinnerEditor = (JSpinner.DefaultEditor) editorFiles;
			spinnerEditor.getTextField().setHorizontalAlignment(JTextField.CENTER);
		}
		if (editorColumnes instanceof JSpinner.DefaultEditor) {
			spinnerEditor2 = (JSpinner.DefaultEditor) editorColumnes;
			spinnerEditor2.getTextField().setHorizontalAlignment(JTextField.CENTER);
		}
		if (editorBombes instanceof JSpinner.DefaultEditor) {
			spinnerEditor3 = (JSpinner.DefaultEditor) editorBombes;
			spinnerEditor3.getTextField().setHorizontalAlignment(JTextField.CENTER);
		}
		// Afegeixo panell, labels i textfields al frame.
		panellPersonalitzatBtns.setLayout(new GridLayout(1,2));
		personalitzat.add(panellPersonalitzat);
		panellPersonalitzat.add(labelFiles);
		panellPersonalitzat.add(spinnerFiles);
		panellPersonalitzat.add(labelColumnes);
		panellPersonalitzat.add(spinnerColumnes);
		panellPersonalitzat.add(labelBombes);
		panellPersonalitzat.add(spinnerBombes);
		panellPersonalitzat.add(panellPersonalitzatBtns);
		panellPersonalitzatBtns.add(botoAcceptar);
		panellPersonalitzatBtns.add(botoCancel);
		botoAcceptar.addActionListener(joc);
		botoCancel.addActionListener(joc);
		// Visiblitat
		personalitzat.pack();
		personalitzat.setVisible(true);

	}

	public void configGraella() {
		panellGraella.setLayout(new GridLayout(FILES, COLUMNES));
		panellGraella.setBackground(Color.WHITE);
		getContentPane().add(panellGraella);
		botoCasellesDestapar.setText(""+(FILES*COLUMNES-BOMBES));
	}
	
	public static int getFilaSpinner() {
        return (int) spinnerFiles.getValue();
    }
	
	public static int getColumnaSpinner() {
        return (int) spinnerColumnes.getValue();
    }
	
	public static int getBombaSpinner() {
        return (int) spinnerBombes.getValue();
    }
	 public void mostrarMissatgeError(String missatge) {
	        JOptionPane.showMessageDialog(rootPane, missatge, "Victoria", JOptionPane.ERROR_MESSAGE);
	    }
	public Boto[][] botons = null;

	public void omplirGraella() {
		botons = new Boto[FILES][COLUMNES];
		for (int i = 0; i < FILES; i++) {
			for (int j = 0; j < COLUMNES; j++) {
				Boto boto = new Boto(i, j);
				// boto.setText(String.valueOf(boto.posicio));

				boto.addMouseListener(joc);
				panellGraella.add(boto);
				botons[i][j] = boto;
			}
		}
	}
public void mostrarMissatge(String missatge) {
		
		JOptionPane.showMessageDialog(rootPane, missatge);
		
	}
	public class Boto extends JButton {

		boolean clicat = false;
		int fila, columna;

		public Boto(int fila, int columna) {
			this.fila = fila;
			this.columna = columna;
		}

		public int getFila() {
			return fila;
		}

		public void setFila(int fila) {
			this.fila = fila;
		}

		public int getColumna() {
			return columna;
		}

		public void setColumna(int columna) {
			this.columna = columna;
		}
		
	

	}

	// public static void main(String[] args) {
	// javax.swing.SwingUtilities.invokeLater(new Runnable() {
	// @Override
	// public void run() {
	// new Principal();
	// }
	// });
	// }
}
